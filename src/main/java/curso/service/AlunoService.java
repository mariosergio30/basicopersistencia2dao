package curso.service;

import java.util.List;

import curso.dao.AlunoDao;
import curso.jdbc.ConexaoJDBC;
import curso.model.Aluno;


public class AlunoService {


	private AlunoDao alunoDao;

	public AlunoService(ConexaoJDBC con) {
		super();

    	alunoDao = new AlunoDao(con);
	}


	// matricula de um novo aluno 
    public boolean matricular(Aluno a) {

		try {
			if (alunoDao.create(a)) {
				return true;
			}
		}
		catch (Exception e) {
			System.out.println("Erro ao matricular " + e.getMessage());
		}


    	return false;
    }
	
		
	
	// saida de um aluno da escola 
	public boolean removeAluno(Aluno a) {

    	
	  	if (alunoDao.delete(a))
			return true;

		 return false;

	}


	public Aluno consultaAlunoPorCpf(String cpf) {

		return alunoDao.readOne(cpf);
	}


	/* retorna a lista completa de Alunos
	 matriculados */
	public List<Aluno> consultaAlunos() {
		return alunoDao.read();
	}

	

}
