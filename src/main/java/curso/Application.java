package curso;

// Importação do Pacote java.sql para acesso a bancos de dados relacionais
import curso.jdbc.ConexaoJDBC;
import curso.model.Aluno;
import curso.service.AlunoService;

import java.util.List;

public class Application {

    static AlunoService alunosService;

    public static void main(String[] args) {

        ConexaoJDBC con = conectaDB();
        alunosService = new AlunoService(con);

        inserirAlunos();

        consultaDisciplinas();

        consultaTodosAlunos();

        consultaUmAluno("777.999.999-99");

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< FIM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    public static ConexaoJDBC conectaDB() {


        // <<<<<<<<  Conecta ao banco de Dados >>>>>>>
        ConexaoJDBC conexao = new ConexaoJDBC();
        if (!conexao.conecta()) {
            System.out.println("Não foi possível conectar ao Banco de Dados !");
            return null;
        }
        System.out.println("Conectado ao Banco de Dados !");

        return conexao;

    }

    public static boolean inserirAlunos() {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de INSERT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

        Aluno aluno1 = new Aluno();
        aluno1.setNome("Aluno Exemplo");
        aluno1.setIdade(21);
        aluno1.setCpf("000.001.003.43");
        aluno1.setMatricula("788667");
        aluno1.setSexo("M");

        if (alunosService.matricular(aluno1)) {
            System.out.println("OK");
            return true;
        }

        return false;

    }


    public static void consultaDisciplinas() {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL Disciplinas  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("Consultando primeira e ultima disciplina...");

        System.out.println("TO DO");

    }




    public static void consultaTodosAlunos() {


        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (com retorno Todos os Registros)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;


        List<Aluno> listAlunos = alunosService.consultaAlunos();

        for(Aluno a : listAlunos){
            System.out.println("Nome " + a.getNome() + " - matricula " + a.getMatricula());

        }


    }


    public static void consultaUmAluno(String cpf) {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (Um Aluno)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

        Aluno aluno = alunosService.consultaAlunoPorCpf(cpf);

        if (aluno != null) {

            System.out.println("Nome: " + aluno.getNome() + " CPF: " + aluno.getCpf());
        }

    }



}
